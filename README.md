# Track block starter and timer

## Description

This project is for acceleration training, particularly for block starts.
Starter component is responsible for start procedure - ready, set and go commands. Set command is given after camera stops detecting movement. Go command is given a random period of time specified either by terminal or in starter/config.py file. To initiate start procedure athlete shall show appropriate hand sign to the camera.
Timer component is optional and is responsible for timing the time of the run. Transmitter and receiver should be placed facing each other. 

Communication between the components is based on http server running on Timers device. For Starter-Timer to work both components need to be on the same local network, eg. mobile WiFi hotspot.

## Hardware

Starter component is placed on a laptop and requires only an USB camera.
Timer is based on Raspberry Pi and requires beam interruption sensor with open collector output.

## Software requirements
###### Starter
* OpenCV
* Mediapipe
* Simpleaudio
* Requests
###### Timer
* Flask
* Redis
* RedisPy

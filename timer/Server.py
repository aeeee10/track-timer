from flask import Flask
from redis import Redis
import multiprocessing
import subprocess
from time import sleep

from timer.Timer import Timer
from ServerRoutes import ServerRoutes
from MessageKeys import MessageKeys

server = Flask(__name__)
redis = Redis(decode_responses=True)
timer = Timer(redis)


@server.route(ServerRoutes.CONNECTION_CHECK.value)
def test_connection():
    return {MessageKeys.CONNECTION.value: True}


@server.route(ServerRoutes.START_READING_FINISH.value)
def start_reading_finish_line():
    timer.start_reading_finish_line()
    return "Started reading finish line."


@server.route(ServerRoutes.FINISH_CHECK.value)
def has_finished():
    key = MessageKeys.HAS_FINISHED.value
    has_finished = redis.get(key)
    return {key: has_finished}


@server.route(ServerRoutes.SHUT_DOWN_RPI.value)
def shutdown():
    subprocess.run(["sudo", "shutdown", "-h", "now"])


@server.route(ServerRoutes.GET_TIME.value)
def get_time():
    key = MessageKeys.FINISH_TIMESTAMP.value
    time = redis.get(key)
    return {key: time}


def start_redis():
    subprocess.call("redis-server")


def run_server():
    process = multiprocessing.Process(target=start_redis)
    process.start()
    sleep(1)
    redis.flushdb()
    server.run("0.0.0.0")

import RPi.GPIO as GPIO
import multiprocessing
from datetime import datetime
from redis import Redis
from MessageKeys import MessageKeys


class Timer:
    def __init__(self, redis: Redis) -> None:
        self.redis = redis
        GPIO.setmode(GPIO.BCM)
        self.input_pin = 2
        GPIO.setup(self.input_pin, GPIO.IN)

    def start_reading_finish_line(self):
        self.redis.set(MessageKeys.HAS_FINISHED.value, 0)
        process = multiprocessing.Process(target=self.read_finish_line)
        process.start()
        process.join()

    def read_finish_line(self):
        while GPIO.input(self.input_pin) == 1:
            pass
        timestamp = datetime.now()
        self.add_time_to_database(timestamp)

    def add_time_to_database(self, timestamp: datetime):
        self.redis.set(MessageKeys.FINISH_TIMESTAMP.value, timestamp.isoformat())
        self.redis.set(MessageKeys.HAS_FINISHED.value, 1)


if __name__ == "__main__":
    try:
        timer = Timer()
        timer.start_reading_finish_line()
    except KeyboardInterrupt:
        GPIO.cleanup()

from enum import Enum


class MessageKeys(Enum):
    CONNECTION = "connection"
    HAS_FINISHED = "has_finished"
    FINISH_TIMESTAMP = "finish_timestamp"

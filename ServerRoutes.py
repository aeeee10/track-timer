from enum import Enum


class ServerRoutes(Enum):
    CONNECTION_CHECK = "/test_connection"
    START_READING_FINISH = "/start_reading_finish"
    FINISH_CHECK = "/has_finished"
    GET_TIME = "/get_time"
    SHUT_DOWN_RPI = "/shutdown"

from enum import Enum


class SystemState(Enum):
    READY = "Ready to start."
    RUN_ONGOING = "Waiting for the run to finish."

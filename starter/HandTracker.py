import mediapipe as mp
from mediapipe.python.solutions.hands import HandLandmark
import cv2 as cv
from enum import Enum, auto


class HandTracker:
    def __init__(self, camera) -> None:
        self.camera = camera
        self.hands = mp.solutions.hands.Hands()

    def watch_for_sign(self) -> Enum:
        sign_detections = 0
        while True:
            _, img = self.camera.read()
            rgb_img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
            results = self.hands.process(rgb_img)
            if results.multi_hand_landmarks:
                for landmarks in results.multi_hand_landmarks:
                    if self._check_for_start_sign(landmarks.landmark):
                        sign_detections += 1
                        if sign_detections > 5:
                            return HandSigns.BEGIN_START
                    elif self._check_for_quit_sign(landmarks.landmark):
                        sign_detections += 1
                        if sign_detections > 5:
                            return HandSigns.EXIT_PROGRAM
                    else:
                        sign_detections = 0

    def _check_for_start_sign(self, landmarks):
        if self._hand_raised(landmarks):
            index_finger: bool = (
                landmarks[HandLandmark.INDEX_FINGER_TIP].y
                > landmarks[HandLandmark.INDEX_FINGER_PIP].y
            )
            middle_finger: bool = (
                landmarks[HandLandmark.MIDDLE_FINGER_TIP].y
                > landmarks[HandLandmark.MIDDLE_FINGER_PIP].y
            )
            ring_finger: bool = (
                landmarks[HandLandmark.RING_FINGER_TIP].y
                < landmarks[HandLandmark.RING_FINGER_PIP].y
            )
            pinky: bool = (
                landmarks[HandLandmark.PINKY_TIP].y
                > landmarks[HandLandmark.PINKY_PIP].y
            )

            return index_finger and middle_finger and ring_finger and pinky
        else:
            index_finger: bool = (
                landmarks[HandLandmark.INDEX_FINGER_TIP].y
                < landmarks[HandLandmark.INDEX_FINGER_PIP].y
            )
            middle_finger: bool = (
                landmarks[HandLandmark.MIDDLE_FINGER_TIP].y
                < landmarks[HandLandmark.MIDDLE_FINGER_PIP].y
            )
            ring_finger: bool = (
                landmarks[HandLandmark.RING_FINGER_TIP].y
                > landmarks[HandLandmark.RING_FINGER_PIP].y
            )
            pinky: bool = (
                landmarks[HandLandmark.PINKY_TIP].y
                < landmarks[HandLandmark.PINKY_PIP].y
            )

            return index_finger and middle_finger and ring_finger and pinky

    def _check_for_quit_sign(self, landmarks):
        return False

    def _hand_raised(self, landmarks):
        return (
            landmarks[HandLandmark.MIDDLE_FINGER_TIP].y
            > landmarks[HandLandmark.WRIST].y
        )


class HandSigns(Enum):
    BEGIN_START = auto()
    EXIT_PROGRAM = auto()

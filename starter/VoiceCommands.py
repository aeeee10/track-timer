from simpleaudio import WaveObject
from .config import Sounds


class VoiceCommands:
    def __init__(self) -> None:
        self._ready_sound = WaveObject.from_wave_file(Sounds.READY.value)
        self._set_sound = WaveObject.from_wave_file(Sounds.SET.value)
        self._go_sound = WaveObject.from_wave_file(Sounds.GO.value)

    def ready(self):
        self._ready_sound.play()

    def set(self):
        self._set_sound.play()

    def go(self):
        self._go_sound.play()

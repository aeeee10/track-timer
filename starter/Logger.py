import os
from starter.RequestSender import RequestSender
from starter.TimeCalculator import TimeCalculator


class Logger:
    def __init__(
        self,
        *,
        request_sender: RequestSender = None,
        time_calculator: TimeCalculator = None,
        starter=None,
    ) -> None:
        self.request_sender = request_sender
        self.time_calculator = time_calculator
        self.starter = starter

    def log(self):
        self._clear()
        self._log_system_state()
        self._log_last_time()
        self._log_connection_status()

    def _log_system_state(self):
        if self.starter:
            print(self.starter.system_state.value)

    def _log_connection_status(self):
        if self.request_sender:
            if self.request_sender.connected:
                print(
                    f"Connected to {self.request_sender.ip}{self.request_sender.port}"
                )
            else:
                print("No connection to Timer.")
        else:
            print("Standalone mode - not connecting to Timer.")

    def _log_last_time(self):
        if self.time_calculator:
            if self.time_calculator.run_time:
                print(f"Last time: {self.time_calculator.run_time:.2f} s")
            else:
                print("No time set yet.")
        else:
            print("Standalone mode - not measuring times.")

    @staticmethod
    def _clear():
        os.system("cls" if os.name == "nt" else "clear")


class NoLogger:
    def log(self):
        pass

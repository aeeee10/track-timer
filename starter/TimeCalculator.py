from datetime import datetime


class TimeCalculator:
    def __init__(self) -> None:
        self.start_time = None
        self.finish_time = None
        self.run_time = None

    def add_start_time(self, start_time: datetime) -> None:
        self.start_time = start_time.isoformat()
        self.run_time = None

    def add_finish_time(self, finish_time: str) -> None:
        self.finish_time = finish_time

    def calculate_run_time(self) -> None:
        """
        Both times are stored as string in ISO8601 format.
        """
        start_times: list[str] = self.start_time.split("T")[-1].split(":")
        finish_times: list[str] = self.finish_time.split("T")[-1].split(":")
        run_time = self.extract_time(finish_times) - self.extract_time(start_times)
        self.add_new_time_to_sheet(run_time)
        self.run_time = run_time

    def extract_time(self, time_strs: list[str]) -> float:
        time = 0
        for unit_power, t in enumerate(reversed(time_strs)):
            time += float(t) * (60**unit_power)
        return time

    def add_new_time_to_sheet(self, time) -> None:
        pass
        # sheet connection here

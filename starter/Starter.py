import cv2 as cv
from datetime import datetime
from time import sleep
import random

from starter.BlockWatcher import BlockWatcher
from starter.HandTracker import HandTracker, HandSigns
from starter.Logger import Logger, NoLogger
from starter.TimeCalculator import TimeCalculator
from starter.VoiceCommands import VoiceCommands
from starter.RequestSender import RequestSender
from starter.SystemStates import SystemState
from starter.config import USB_ID, AFTER_READY_SLEEP


class Starter:
    def __init__(self, args) -> None:
        self.read_optional_arguments(args)
        self.camera = cv.VideoCapture(USB_ID)
        self.block_watcher = BlockWatcher(self.camera)
        self.hand_tracker = HandTracker(self.camera)
        self.voice_commands = VoiceCommands()
        if self.use_timer:
            self.request_sender = RequestSender()
            self.time_calculator = TimeCalculator()
        else:
            self.request_sender = None
            self.time_calculator = None
        if self.use_logger:
            self.logger = Logger(
                request_sender=self.request_sender,
                time_calculator=self.time_calculator,
                starter=self,
            )
        else:
            self.logger = NoLogger()
        self.system_state = SystemState.READY
        print("Starter initialized.")

    def read_optional_arguments(self, args):
        self.use_timer: bool = args.timer
        self.use_logger: bool = args.logger
        self.set_times: list[int] = args.set_times

    def main_loop(self) -> None:
        while True:
            self.logger.log()
            sign = self.hand_tracker.watch_for_sign()
            if sign == HandSigns.BEGIN_START:
                self.start_procedure()
            elif sign == HandSigns.EXIT_PROGRAM:
                break

    def start_procedure(self):
        """
        Block start procedure.
        """
        self.system_state = SystemState.RUN_ONGOING
        self.voice_commands.ready()
        sleep(AFTER_READY_SLEEP)
        if self.block_watcher.no_movement():
            if self.use_timer:
                self.request_sender.notify_about_start()
            self.start()
            if self.use_timer:
                self.wait_for_finish()
            self.system_state = SystemState.READY

    def start(self) -> None:
        """
        Part of the start procedure in  which the runner is already in the blocks.
        """
        self.voice_commands.set()
        sleep(random.uniform(*self.set_times))
        start_time = datetime.now()
        self.voice_commands.go()
        if self.use_timer:
            self.time_calculator.add_start_time(start_time)

    def wait_for_finish(self) -> None:
        """
        Waits for runner to finish the run, then calculates the run time.
        """
        while self.request_sender.run_not_finished():
            pass
        finish_time = self.request_sender.get_finish_time()
        self.time_calculator.add_finish_time(finish_time)
        self.time_calculator.calculate_run_time()

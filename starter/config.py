from enum import Enum

# Starter
USB_ID = 2
AFTER_READY_SLEEP = 5.0
MIN_SET_TIME = 3
MAX_SET_TIME = 5
# BlockWatcher
MOVEMENT_THRESHOLD = 40
KERNEL = (7, 7)
ITERATIONS = 3
NOISE_THRESHOLD = 10
MAX_VALUE = 255
# RequestSender
TIMEOUT = 1
# VoiceCommands
class Sounds(Enum):
    READY = "sounds/ready.wav"
    SET = "sounds/set.wav"
    GO = "sounds/go.wav"

import cv2 as cv
from .config import MAX_VALUE, MOVEMENT_THRESHOLD, KERNEL, NOISE_THRESHOLD, ITERATIONS
import numpy


class BlockWatcher:
    def __init__(self, camera) -> None:
        self.camera = camera

    def no_movement(self) -> bool:
        """
        Checks if there is movement in the frame by calculating sum of white pixels on difference of two frames.
        """
        while True:
            _, frame1 = self.camera.read()
            _, frame2 = self.camera.read()
            gray1 = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
            gray2 = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
            difference = cv.absdiff(gray1, gray2)
            _, threshold = cv.threshold(
                difference, MOVEMENT_THRESHOLD, MAX_VALUE, cv.THRESH_BINARY
            )
            opened_img = self._open_image(threshold)
            sum_of_white = numpy.sum(opened_img == MAX_VALUE)
            if self._not_noise(sum_of_white):
                return True

    @staticmethod
    def _open_image(image: numpy.ndarray) -> numpy.ndarray:
        eroded = cv.erode(image, KERNEL, iterations=ITERATIONS)
        dilated = cv.dilate(eroded, KERNEL, iterations=ITERATIONS)

        return dilated

    @staticmethod
    def _not_noise(sum_of_white):
        return sum_of_white < NOISE_THRESHOLD

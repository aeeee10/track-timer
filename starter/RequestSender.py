import requests
import multiprocessing
from time import sleep

from ServerRoutes import ServerRoutes
from MessageKeys import MessageKeys


class RequestSender:
    def __init__(self) -> None:
        self.connected = False
        self.protocol = "http://"
        self.ip = None
        self.port = None
        self.url = None
        self.find_ip()
        self.monitor_connection()

    def notify_about_start(self):
        """
        Notifies the Timer so that it starts reading finish lines IR beam interruption sensor.
        Try-except with short timeout is a workaround to make the request non-blocking.
        """
        try:
            requests.get(
                self.url + ServerRoutes.START_READING_FINISH.value, timeout=0.1
            )
        except (requests.ConnectTimeout, requests.ReadTimeout):
            pass

    def shut_down_timer(self):
        requests.get(self.url + ServerRoutes.SHUT_DOWN_RPI.value)

    def run_not_finished(self):
        response = requests.get(self.url + ServerRoutes.FINISH_CHECK.value).json()
        if int(response[MessageKeys.HAS_FINISHED.value]) == 0:
            return True
        else:
            return False

    def get_finish_time(self) -> str:
        response = requests.get(self.url + ServerRoutes.GET_TIME.value).json()
        return response[MessageKeys.FINISH_TIMESTAMP.value]

    def find_ip(self):
        base_of_ip = "192.168.0."
        port = ":5000"
        for end_of_ip in range(100, 120):
            url = (
                self.protocol
                + base_of_ip
                + str(end_of_ip)
                + port
                + ServerRoutes.CONNECTION_CHECK.value
            )
            try:
                request = requests.get(url, timeout=1).json()
                if request[MessageKeys.CONNECTION.value]:
                    self.ip = base_of_ip + str(end_of_ip)
                    self.port = port
                    self.url = self.protocol + self.ip + self.port
                    self.connected = True
                    print(f"Connected to Timer at {self.ip}{self.port}")
                    break
            except (requests.ConnectTimeout, requests.ConnectionError) as e:
                print(f"Connection timed out: {e}")

    def monitor_connection(self):
        process = multiprocessing.Process(target=self.connection_check)
        process.start()

    def connection_check(self) -> bool:
        while True:
            try:
                request = requests.get(
                    self.protocol
                    + self.ip
                    + self.port
                    + ServerRoutes.CONNECTION_CHECK.value,
                    timeout=1,
                )
                if request.json()[MessageKeys.CONNECTION.value]:
                    self.connected = True
            except (requests.ConnectTimeout, requests.ConnectionError) as e:
                self.connected = False
            sleep(1)

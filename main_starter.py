from argparse import ArgumentParser, BooleanOptionalAction
from starter.Starter import Starter
from starter.config import MIN_SET_TIME, MAX_SET_TIME


def parse_args() -> bool:
    parser = ArgumentParser(
        description="Run starter component of track block starter and timer."
    )
    parser.add_argument(
        "--timer",
        action=BooleanOptionalAction,
        default=True,
        help="Use timing component.",
    )
    parser.add_argument(
        "--logger",
        default=True,
        action=BooleanOptionalAction,
        help="Use logging component [DEBUG FEATURE].",
    )
    parser.add_argument(
        "-s",
        "--set-times",
        nargs=2,
        type=int,
        default=[MIN_SET_TIME, MAX_SET_TIME],
        metavar=("min_set_time", "max_set_time"),
        help=f"Minimal and maximal time that runner will be held on SET command. Default = [{MIN_SET_TIME}, {MAX_SET_TIME}], set in starter/config.py",
    )
    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = parse_args()
    starter = Starter(args)
    starter.main_loop()
